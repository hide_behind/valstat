﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace HideBehind.ValstatWpfDotNet45Rp
{
  public class Statistics
  {
    public Statistics(IEnumerable<Match> collection, Player Target)
    {
      Subtotal = collection.Count();
      Wins = collection.Count(x => x.WinnerName == Target.Name);
      Losses = collection.Count(x => x.WinnerName != Target.Name && !String.IsNullOrEmpty(x.WinnerName));
      Draws = collection.Count(x => String.IsNullOrEmpty(x.WinnerName));
      WinningPercentage = Math.Round(100d * (double)Wins / (double)Subtotal, 1);
      From = collection.ElementAt(Subtotal - 1).Date;
      To = collection.First().Date;
    }

    public int Subtotal { get; private set; }

    public int Wins { get; private set; }

    public int Losses { get; private set; }

    public int Draws { get; private set; }

    public double WinningPercentage { get; private set; }

    public DateTime From { get; private set; }

    public DateTime To { get; private set; }
  }
}
