﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace HideBehind.ValstatWpfDotNet45Rp
{
  public class Stage
  {
    public static readonly Stage Any = new Stage { _id = 0, _longhand = "指定しない", _shorthand = "ANY" };
    public static readonly Stage Ai = new Stage { _id = 1, _longhand = "SPACE ELEVATOR", _shorthand = "AI" };
    public static readonly Stage Na = new Stage { _id = 2, _longhand = "AIRCRAFT CARRIER", _shorthand = "NA" };
    public static readonly Stage Me = new Stage { _id = 3, _longhand = "ANCIENT RUINS", _shorthand = "ME" };
    public static readonly Stage Se = new Stage { _id = 4, _longhand = "SHINTO SHRINE", _shorthand = "SE" };
    public static readonly Stage Ma = new Stage { _id = 5, _longhand = "SECRET SANCTUARY", _shorthand = "MA" };
    public static readonly Stage Ay = new Stage { _id = 6, _longhand = "MILITARY TRAIN", _shorthand = "AY" };
    public static readonly Stage Re = new Stage { _id = 7, _longhand = "TROPICAL BEACH", _shorthand = "RE" };
    public static readonly Stage El = new Stage { _id = 8, _longhand = "MIDNIGHT CITY", _shorthand = "EL" };

    private int _id;

    public int Id
    {
      get { return _id; }
    }

    private string _longhand;

    public string Longhand
    {
      get { return _longhand; }
    }

    private string _shorthand;

    public string Shorthand
    {
      get { return _shorthand; }
    }

    private static readonly ReadOnlyCollection<Stage> _list
      = new ReadOnlyCollection<Stage>(new[] { Any, Ai, Na, Me, Se, Ma, Ay, Re, El, });

    public static ReadOnlyCollection<Stage> List
    {
      get { return _list; }
    }
  }
}
