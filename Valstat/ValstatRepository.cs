﻿using HideBehind.ValstatWpfDotNet45Rp.Common;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml;

namespace HideBehind.ValstatWpfDotNet45Rp
{
  public class ValstatRepository : BindableBase
  {
    public ValstatRepository()
    {
      string exePath = Environment.GetCommandLineArgs()[0];
      FileVersionInfo fileVersionInfo = FileVersionInfo.GetVersionInfo(exePath);
      this.AppNameAndVersion = fileVersionInfo.ProductName + " Version " + fileVersionInfo.ProductVersion;
      this.startupPath = Path.GetDirectoryName(Path.GetFullPath(exePath));
      this.MatchRepository = new ObservableCollection<Match>();
      this.PlayerList = new ObservableCollection<Player>();
      this.PlayerList.CollectionChanged += this.Players_CollectionChanged;
      this.LoadSetting();
      this.LoadDatabase();
      this.RefreshPlayerList();
      this.IsStandby = true;
    }

    private void LoadSetting()
    {
      this.dbfi = new FileInfo(startupPath + "\\ValstatDatabase.xml");
      this.ReplayDirPath = Properties.Settings.Default.ReplayDirPath;
      this.IsTimeLimitEnabled = Properties.Settings.Default.IsTimeLimitEnabled;
      if (this.IsTimeLimitEnabled)
      {
        this.TimeLimit = Properties.Settings.Default.TimeLimit;
      }
    }

    private void LoadDatabase()
    {
      if (!this.dbfi.Exists)
      {
        this.CreateDatabase();
      }
      else
      {
        this.databaseXml.Load(startupPath + "\\ValstatDatabase.xml");
        if (this.IsDatabaseCompatibilityBroken())
        {
          this.RefreshDatabase("\\REGISTERED");
        }
        else
        {
          XmlNodeList matchNodeList = databaseXml.SelectNodes(matchXPath);
          int nMatch = matchNodeList.Count;
          this.DatabaseStatusMessage
            = "最終更新日時: " + this.dbfi.LastWriteTime + "; "
            + "登録件数: " + nMatch + ".";
          foreach (XmlNode x in matchNodeList)
          {
            try
            {
              MatchRepository.Add(Match.ParseNode(x, this.ReplayDirPath + "\\REGISTERED"));
            }
            catch
            {
              return;
            }
          }
        }
      }
    }

    private bool IsDatabaseCompatibilityBroken()
    {
      string databaseVersionXPath = "/r[@g='valforce']/@v";
      if (this.databaseXml.SelectSingleNode(databaseVersionXPath) != null)
      {
        return this.databaseXml.SelectSingleNode(databaseVersionXPath).Value != "1.03";
      }
      return true;
    }

    private void CreateDatabase()
    {
      this.databaseXml = new XmlDocument();
      XmlDeclaration d = databaseXml.CreateXmlDeclaration("1.0", Encoding.UTF8.BodyName, null);
      this.databaseXml.AppendChild(d);
      XmlElement e = databaseXml.CreateElement("r");
      e.SetAttribute("g", "valforce");
      e.SetAttribute("v", "1.03");
      this.databaseXml.AppendChild(e);
      this.databaseXml.Save(this.startupPath + "\\ValstatDatabase.xml");
      this.dbfi.Refresh();
      this.LoadDatabase();
    }

    public void RefreshDatabase(string dirName)
    {
      this.timer = Stopwatch.StartNew();
      bool isRefreshMode = dirName == "\\REGISTERED";
      if (isRefreshMode)
      {
        this.CreateDatabase();
      }
      DirectoryInfo di = new DirectoryInfo(this.ReplayDirPath + dirName);
      if (!di.Exists)
      {
        this.ReportDirectoryNotExists(di);
      }
      else
      {
        this.R1(di);
      }
      this.Progress = 0;
      if (isRefreshMode)
      {
        Process.Start(this.startupPath + "\\Valstat.exe");
      }
    }

    private void R1(DirectoryInfo di)
    {
      IEnumerable<FileInfo> fis = di.EnumerateFiles("*.rep", SearchOption.AllDirectories)
        .OrderBy(x => x.CreationTime);
      int count = fis.Count<FileInfo>();
      if (count == 0)
      {
        this.ReportNoNeedToUpdate();
      }
      else
      {
        double step = 100d / (double)count;
        if (di.FullName == this.ReplayDirPath + "\\REGISTERED")
        {
          this.R2(fis, step);
        }
        else
        {
          this.U2(fis, step);
        }
      }
    }

    private void R2(IEnumerable<FileInfo> fis, double step)
    {
      double nSuccess = 0d;
      int nFailure = 0;
      foreach (FileInfo fi in fis)
      {
        R3(fi, ref nSuccess, ref nFailure);
        this.Progress += step;
        this.ReportDatabaseStatus(false, nSuccess, nFailure);
      }
      this.databaseXml.Save(this.startupPath + "\\ValstatDatabase.xml");
      this.dbfi.Refresh();
      this.ReportDatabaseStatus(false, nSuccess, nFailure);
    }

    private void U2(IEnumerable<FileInfo> fis, double step)
    {
      bool isTimeOut = false;
      var nSuccess = 0d;
      int nFailure = 0;
      foreach (FileInfo fi in fis)
      {
        U3(fi, ref nSuccess, ref nFailure);
        this.Progress += step;
        this.ReportDatabaseStatus(isTimeOut, nSuccess, nFailure);
        if (this.timer.Elapsed.TotalSeconds > this.TimeLimit && this.IsTimeLimitEnabled)
        {
          this.Progress = 100;
          isTimeOut = true;
          break;
        }
      }
      this.databaseXml.Save(this.startupPath + "\\ValstatDatabase.xml");
      this.dbfi.Refresh();
      this.ReportDatabaseStatus(isTimeOut, nSuccess, nFailure);
    }

    private void R3(FileInfo fi, ref double nSuccess, ref int nFailure)
    {
      Match matchInstance;
      try
      {
        matchInstance = ParseRep(fi);
        InsertMatchElement(matchInstance);
        nSuccess++;
      }
      catch (Exception)
      {
        nFailure++;
      }
    }

    private void U3(FileInfo fi, ref double nSuccess, ref int nFailure)
    {
      Match matchInstance;
      string destinationPath;
      bool condition;
      try
      {
        matchInstance = ParseRep(fi);
        matchInstance.FilePath = GetVersionAwarePath(matchInstance);
        destinationPath = matchInstance.FilePath;
        condition = true;
      }
      catch (Exception)
      {
        matchInstance = null;
        destinationPath = fi.FullName.Replace(this.ReplayDirPath, this.ReplayDirPath + "\\OTHER");
        condition = false;
      }

      try
      {
        DirectoryInfo destinationParentDi = Directory.GetParent(destinationPath);
        if (!destinationParentDi.Exists)
        {
          destinationParentDi.Create();
        }

        fi.MoveTo(destinationPath);
        if (condition)
        {
          MatchRepository.Insert(0, matchInstance);
          InsertMatchElement(matchInstance);
          UpdatePlayers(matchInstance.P1Name);
          UpdatePlayers(matchInstance.P2Name);
        }
        nSuccess++;
      }
      catch (Exception)
      {
        nFailure++;
      }
    }

    private void UpdatePlayers(string name)
    {
      if (this.PlayerList.Any(x => x.Name == name))
      {
        int index = this.PlayerList.IndexOf(this.PlayerList.First(x => x.Name == name));
        this.PlayerList[index] = new Player { Name = name, Frequency = this.PlayerList[index].Frequency + 1 };
      }
      else
      {
        this.PlayerList.Add(new Player { Name = name, Frequency = 1 });
      }
    }

    private void ReportDirectoryNotExists(DirectoryInfo di)
    {
      this.DatabaseStatusMessage = "エラー: " + this.ReplayDirPath + " に " + di.Name + " が存在しません";
    }

    private void ReportNoNeedToUpdate()
    {
      this.dbfi.Refresh();
      this.DatabaseStatusMessage
        = "更新の必要はありません; "
        + "最終更新日時: " + this.dbfi.LastWriteTime + "; "
        + "登録件数: " + this.databaseXml.SelectNodes(matchXPath).Count + ".";
    }


    private void ReportDatabaseStatus(bool isTimeOut, double nSuccess, int nFail)
    {
      this.DatabaseStatusMessage
        = (isTimeOut ? "タイムアウトしたので再度「データベースの更新」をして下さい; " : String.Empty)
        + "最終更新日時: " + this.dbfi.LastWriteTime + "; "
        + "更新時間: " + this.timer.Elapsed.TotalSeconds.ToString("F3") + "秒; "
        + "登録件数: " + this.databaseXml.SelectNodes(matchXPath).Count
        + " (成功: " + nSuccess + "; 失敗: " + nFail + ").";
    }

    public void SaveSetting()
    {
      Properties.Settings.Default.ReplayDirPath = this.ReplayDirPath;
      Properties.Settings.Default.IsTimeLimitEnabled = this.IsTimeLimitEnabled;
      Properties.Settings.Default.TimeLimit = this.TimeLimit;
      Properties.Settings.Default.Save();
    }

    public void RefreshPlayerList()
    {
      foreach (Match item in this.MatchRepository)
      {
        this.UpdatePlayers(item.P1Name);
        this.UpdatePlayers(item.P2Name);
      }
    }

    public void OpenReplay(Match selectedMatch)
    {
      switch (selectedMatch.GameVersion.Shorthand)
      {
        case "2.04":
          Process.Start(selectedMatch.FilePath);
          break;
        case "2.03":
          Process.Start(Directory.GetParent(this.ReplayDirPath).FullName + "\\valforce203.exe",
            selectedMatch.FilePath);
          break;
        default:
          throw new ArgumentException();
      }
    }

    private void InsertMatchElement(Match matchInstance)
    {
      XmlElement m = this.databaseXml.CreateElement("m");
      m.SetAttribute("d", matchInstance.Date.ToString());
      m.SetAttribute("f", matchInstance.FilePath
        .Replace(this.ReplayDirPath + "\\REGISTERED\\V" + matchInstance.GameVersion.Shorthand, String.Empty));
      m.SetAttribute("v", matchInstance.GameVersion.Id.ToString());
      m.SetAttribute("m", matchInstance.Mode.Id.ToString());
      m.SetAttribute("p", matchInstance.P1Character.Id.ToString());
      m.SetAttribute("n", matchInstance.P1Name);
      m.SetAttribute("q", matchInstance.P2Character.Id.ToString());
      m.SetAttribute("o", matchInstance.P2Name);
      m.SetAttribute("s", matchInstance.Stage.Id.ToString());
      m.SetAttribute("w", matchInstance.WinnerName);
      XmlElement r = this.databaseXml.DocumentElement;
      r.InsertBefore(m, r.FirstChild);
    }

    private Match ParseRep(FileInfo fi)
    {
      string[] line = ReadRep(fi);
      return new Match(
        GameVersion.List.First(x => x.Shorthand == line[1]),
        DateTime.Parse(line[2]),
        Mode.List.First(x => x.Shorthand == line[3]),
        line[4],
        line[5],
        Character.List.First(x => x.Shorthand == line[6]),
        Character.List.First(x => x.Shorthand == line[7]),
        Stage.List.First(x => x.Longhand == line[8]),
        line[9],
        fi.FullName
      );
    }

    private static string[] ReadRep(FileInfo fi)
    {
      var header = new ReadOnlyCollection<string>(new[]
      {
        "#game=", "#version=", "#date=", "#mode=", "#1,name=", "#2,name=",
        "#1,character=" ,"#2,character=","#stage=", "#winner=", "#loser=",
      });
      int k = header.Count;
      var line = new string[k];
      StreamReader sr = null;
      try
      {
        sr = fi.OpenText();
        line[0] = sr.ReadLine();
        if (line[0] == "#game=valforce")
        {
          for (int i = 1; i < k; i++)
          {
            line[i] = sr.ReadLine();
            var regex = new Regex(header[i]);
            line[i] = regex.Replace(line[i], String.Empty);
          }
        }
      }
      finally
      {
        if (sr != null)
        {
          sr.Close();
        }
      }
      return line;
    }

    private string GetVersionAwarePath(Match matchInstance)
    {
      return matchInstance.FilePath.Replace(this.ReplayDirPath, this.ReplayDirPath + "\\REGISTERED\\V" + matchInstance.GameVersion.Shorthand);
    }

    private Stopwatch timer;

    private static readonly string matchXPath = "/r[@g='valforce'][@v='1.03']/m";

    private readonly string startupPath;

    private XmlDocument databaseXml = new XmlDocument();

    private FileInfo dbfi;

    public ObservableCollection<Match> MatchRepository { get; private set; }

    public ObservableCollection<Player> PlayerList { get; private set; }

    void Players_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
    {
      if (e.OldItems != null)
      {
        foreach (Object item in e.OldItems)
        {
          ((INotifyPropertyChanged)item).PropertyChanged -= ItemPropertyChanged;
        }
      }
      if (e.NewItems != null)
      {
        foreach (Object item in e.NewItems)
        {
          ((INotifyPropertyChanged)item).PropertyChanged += ItemPropertyChanged;
        }
      }
    }

    private void ItemPropertyChanged(object sender, PropertyChangedEventArgs e)
    {
      NotifyCollectionChangedEventArgs args
        = new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Replace, sender, sender);
      this.Players_CollectionChanged(this, args);
    }

    public Player this[int index]
    {
      get { return this.PlayerList[index]; }
      private set
      {
        this.PlayerList[index] = value;
        NotifyCollectionChangedEventArgs args
          = new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Replace, this, index);
        this.Players_CollectionChanged(this, args);
      }
    }

    private string _appNameAndVersion;

    public string AppNameAndVersion
    {
      get { return this._appNameAndVersion; }
      private set
      {
        this.SetProperty(ref this._appNameAndVersion, value);
      }
    }

    private string _databaseStatusMessage;

    public string DatabaseStatusMessage
    {
      get { return this._databaseStatusMessage; }
      private set
      {
        this.SetProperty(ref this._databaseStatusMessage, value);
      }
    }

    private bool _isStandby = false;

    public bool IsStandby
    {
      get { return this._isStandby; }
      private set
      {
        this.SetProperty(ref this._isStandby, value);
      }
    }

    private bool _isTimeLimitEnabled;

    public bool IsTimeLimitEnabled
    {
      get { return this._isTimeLimitEnabled; }
      private set
      {
        this.SetProperty(ref this._isTimeLimitEnabled, value);
      }
    }

    private double _progress = 0;

    public double Progress
    {
      get { return this._progress; }
      private set
      {
        this.SetProperty(ref this._progress, value);
      }
    }

    private string _replayDirPath;

    public string ReplayDirPath
    {
      get { return this._replayDirPath; }
      private set
      {
        this.SetProperty(ref this._replayDirPath, value);
      }
    }

    private double _timeLimit;

    public double TimeLimit
    {
      get { return this._timeLimit; }
      private set
      {
        this.SetProperty(ref this._timeLimit, value);
      }
    }
  }
}