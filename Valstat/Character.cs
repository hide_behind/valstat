﻿using System.Collections.ObjectModel;

namespace HideBehind.ValstatWpfDotNet45Rp
{
  public class Character
  {
    public static readonly Character Any = new Character { _id = 0, _longhand = "指定しない", _shorthand = "ANY" };
    public static readonly Character Ai = new Character { _id = 1, _longhand = "神凪アイ", _shorthand = "AI" };
    public static readonly Character Na = new Character { _id = 2, _longhand = "黒羽ナガレ", _shorthand = "NA" };
    public static readonly Character Me = new Character { _id = 3, _longhand = "宝鏡メイ", _shorthand = "ME" };
    public static readonly Character Se = new Character { _id = 4, _longhand = "斑鳩セツナ", _shorthand = "SE" };
    public static readonly Character Ma = new Character { _id = 5, _longhand = "六堂マリア", _shorthand = "MA" };
    public static readonly Character Ay = new Character { _id = 6, _longhand = "月影アヤカ", _shorthand = "AY" };
    public static readonly Character Re = new Character { _id = 7, _longhand = "春風レン", _shorthand = "RE" };
    public static readonly Character El = new Character { _id = 8, _longhand = "美澤エレナ", _shorthand = "EL" };

    private int _id;

    public int Id
    {
      get { return _id; }
    }

    private string _longhand;

    public string Longhand
    {
      get { return _longhand; }
    }

    private string _shorthand;

    public string Shorthand
    {
      get { return _shorthand; }
    }

    private static readonly ReadOnlyCollection<Character> _list
      = new ReadOnlyCollection<Character>(new[] { Any, Ai, Na, Me, Se, Ma, Ay, Re, El, });

    public static ReadOnlyCollection<Character> List
    {
      get { return _list; }
    }

    public static string Annotation(Character character)
    {
      if (character.Id != 0)
      {
        return " (" + character.Shorthand + ")";
      }
      else
      {
        return "";
      }
    }

  }
}
