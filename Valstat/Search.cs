﻿using HideBehind.ValstatWpfDotNet45Rp.Common;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;

namespace HideBehind.ValstatWpfDotNet45Rp
{
  public class Search : BindableBase
  {
    public Search()
    {
      this.Repository = new ValstatRepository();
      this.MatchList = new ObservableCollection<Match>();
      this.StatisticsList = new ObservableCollection<Statistics>();
      this.LoadSearch();
    }

    private void LoadSearch()
    {
      this.IsOpponentNameChecked = Properties.Search.Default.IsOpponentNameChecked;
      if (this.Repository.PlayerList != null)
      {
        if (!String.IsNullOrEmpty(Properties.Search.Default.TargetName))
        {
          this.TargetPlayer = this.Repository.PlayerList.FirstOrDefault((x) => x.Name == Properties.Search.Default.TargetName);
        }
        if (!String.IsNullOrEmpty(Properties.Search.Default.OpponentName) && IsOpponentNameChecked)
        {
          this.OpponentPlayer = this.Repository.PlayerList.FirstOrDefault((x) => x.Name == Properties.Search.Default.OpponentName);
        }
      }
      this.TargetCharacter = Character.List[Properties.Search.Default.TargetCharacterId];
      this.OpponentCharacter = Character.List[Properties.Search.Default.OpponentCharacterId];
      this.IsStartDateChecked = Properties.Search.Default.IsStartDateChecked;
      if (Properties.Search.Default.StartDate == null || !IsStartDateChecked)
      {
        this.StartDate = new DateTime(2011, 4, 3);
      }
      else
      {
        this.StartDate = Properties.Search.Default.StartDate;
      }
      this.IsEndDateChecked = Properties.Search.Default.IsEndDateChecked;
      if (!this.IsEndDateChecked || Properties.Search.Default.EndDate == null)
      {
        this.EndDate = DateTime.Today;
      }
      else
      {
        this.EndDate = Properties.Search.Default.EndDate;
      }
      this.SelectedStage = Stage.List[Properties.Search.Default.SelectedStageId];
      this.SelectedGameVersion = GameVersion.List[Properties.Search.Default.SelectedGameVersionId];
      this.SelectedResult = Result.List[Properties.Search.Default.SelectedResultId];
      this.IsAnalysisEnabled = Properties.Search.Default.IsAnalysisEnabled;
      this.AggregationUnitSize = Properties.Search.Default.AggregationUnitSize;
      this.IsReplayHelperEnabled = Properties.Search.Default.IsReplayHelperEnabled;
    }

    public void SaveSearch()
    {
      if (this.TargetPlayer == null)
      {
        Properties.Search.Default.TargetName = null;
      }
      else
      {
        Properties.Search.Default.TargetName = this.TargetPlayer.Name;
      }
      Properties.Search.Default.TargetCharacterId = this.TargetCharacter.Id;
      Properties.Search.Default.IsOpponentNameChecked = this.IsOpponentNameChecked;
      if (this.OpponentPlayer == null)
      {
        Properties.Search.Default.OpponentName = null;
      }
      else
      {
        Properties.Search.Default.OpponentName = this.OpponentPlayer.Name;
      }
      Properties.Search.Default.OpponentCharacterId = this.OpponentCharacter.Id;
      Properties.Search.Default.IsStartDateChecked = this.IsStartDateChecked;
      Properties.Search.Default.StartDate = this.StartDate;
      Properties.Search.Default.IsEndDateChecked = this.IsEndDateChecked;
      Properties.Search.Default.EndDate = this.EndDate;
      Properties.Search.Default.SelectedStageId = this.SelectedStage.Id;
      Properties.Search.Default.SelectedGameVersionId = this.SelectedGameVersion.Id;
      Properties.Search.Default.SelectedResultId = this.SelectedResult.Id;
      Properties.Search.Default.IsAnalysisEnabled = this.IsAnalysisEnabled;
      Properties.Search.Default.IsReplayHelperEnabled = this.IsReplayHelperEnabled;
      Properties.Search.Default.AggregationUnitSize = this.AggregationUnitSize;
      Properties.Search.Default.Save();
    }

    public void Aggregate()
    {
      timer = Stopwatch.StartNew();
      var temp = GetFilteredMatchList();
      var totalStatistics = new Statistics(temp, TargetPlayer);
      if (IsAnalysisEnabled)
      {
        AnalyseStatistics(temp);
      }
      if (IsReplayHelperEnabled)
      {
        LimitToSelectedResult(temp);
      }
      this.ResultMessage = GetResultMessage(totalStatistics);
    }

    private IEnumerable<Match> GetFilteredMatchList()
    {
      return this.Repository.MatchRepository.Where(x =>
        (
          (
            (TargetCharacter.Id != Character.Any.Id ? x.P1Character == TargetCharacter : true) &&
            (x.P1Name == TargetPlayer.Name) &&
            (OpponentCharacter.Id != Character.Any.Id ? x.P2Character == OpponentCharacter : true) &&
            (IsOpponentNameChecked ? x.P2Name == OpponentPlayer.Name : true)
          )
          ||
          (
            (TargetCharacter.Id != Character.Any.Id ? x.P2Character == TargetCharacter : true) &&
            (x.P2Name == TargetPlayer.Name) &&
            (OpponentCharacter.Id != Character.Any.Id ? x.P1Character == OpponentCharacter : true) &&
            (IsOpponentNameChecked ? x.P1Name == OpponentPlayer.Name : true)
          )
        ) &&
        (IsStartDateChecked ? x.Date >= StartDate : true) &&
        (IsEndDateChecked ? x.Date <= EndDate : true) &&
        (SelectedStage.Id != Stage.Any.Id ? x.Stage == SelectedStage : true) &&
        (SelectedGameVersion.Id != GameVersion.Any.Id ? x.GameVersion == SelectedGameVersion : true));
    }

    private void AnalyseStatistics(IEnumerable<Match> ItemsToBeDone)
    {
      this.StatisticsList.Clear();
      while (ItemsToBeDone.Any())
      {
        this.StatisticsList.Add(new Statistics(ItemsToBeDone.Take(this.AggregationUnitSize), this.TargetPlayer));
        ItemsToBeDone = ItemsToBeDone.Skip(AggregationUnitSize);
      }
    }

    private void LimitToSelectedResult(IEnumerable<Match> temp)
    {
      this.MatchList.Clear();
      if (this.SelectedResult == Result.Any)
      {
        foreach (var m in temp)
        {
          this.MatchList.Add(m);
        }
      }
      else if (this.SelectedResult == Result.Draw)
      {
        foreach (var m in temp.Where(x => String.IsNullOrEmpty(x.WinnerName)))
        {
          this.MatchList.Add(m);
        }
      }
      else if (this.SelectedResult == Result.Win)
      {
        foreach (var m in temp.Where(x => x.WinnerName == TargetPlayer.Name))
        {
          this.MatchList.Add(m);
        }
      }
      else if (this.SelectedResult == Result.Loss)
      {
        foreach (var m in temp.Where(x => x.WinnerName != TargetPlayer.Name && !String.IsNullOrEmpty(x.WinnerName)))
        {
          this.MatchList.Add(m);
        }
      }
      else
      {
        throw new Exception();
      }
    }

    private string GetResultMessage(Statistics totalStatistics)
    {
      string tc = Character.Annotation(this.TargetCharacter);
      string oc = Character.Annotation(this.OpponentCharacter);
      string dc = GetDurationCondition();

      return "[結果] " + this.TargetPlayer.Name + tc + " の勝率は、" + totalStatistics.WinningPercentage + "% ("
        + totalStatistics.Subtotal + " 戦 " + totalStatistics.Wins + " 勝 " + totalStatistics.Losses + " 敗 "
        + totalStatistics.Draws + " 分) です; 集計時間: " + timer.Elapsed.TotalMilliseconds.ToString("F3")
        + " ms.\n[条件] 対戦相手: " + OpponentNameAnnotation() + oc + dc + "; ステージ: " + this.SelectedStage.Longhand
        + "; バージョン: " + this.SelectedGameVersion.Longhand + ".";
    }

    private string OpponentNameAnnotation()
    {
      return this.IsOpponentNameChecked && OpponentPlayer != null ? OpponentPlayer.Name : "指定しない";
    }

    private string GetDurationCondition()
    {
      string toEndDateText = " まで";
      string durationLabel = "; 集計期間: ";
      if (this.IsStartDateChecked)
      {
        if (this.IsEndDateChecked)
        {
          return durationLabel + this.StartDate.ToShortDateString() + " から "
            + this.EndDate.ToShortDateString() + toEndDateText;
        }
        else
        {
          return durationLabel + this.StartDate.ToShortDateString() + " から";
        }
      }
      else if (this.IsEndDateChecked)
      {
        return durationLabel + this.EndDate.ToShortDateString() + toEndDateText;
      }
      else
      {
        return "; 集計期間: 指定しない";
      }
    }

    public ObservableCollection<Statistics> StatisticsList { get; private set; }

    public ObservableCollection<Match> MatchList { get; private set; }

    private Stopwatch timer;

    private DateTime _endDate;

    public DateTime EndDate
    {
      get { return this._endDate; }
      private set { this.SetProperty(ref this._endDate, value); }
    }

    private bool _isAnalysisEnabled;

    public bool IsAnalysisEnabled
    {
      get { return this._isAnalysisEnabled; }
      private set { this.SetProperty(ref this._isAnalysisEnabled, value); }
    }

    private bool _isEndDateChecked;

    public bool IsEndDateChecked
    {
      get { return this._isEndDateChecked; }
      private set { this.SetProperty(ref this._isEndDateChecked, value); }
    }

    private bool _isOpponentNameChecked;

    public bool IsOpponentNameChecked
    {
      get { return this._isOpponentNameChecked; }
      private set { this.SetProperty(ref this._isOpponentNameChecked, value); }
    }

    private bool _isReplayHelperEnabled;

    public bool IsReplayHelperEnabled
    {
      get { return this._isReplayHelperEnabled; }
      private set { this.SetProperty(ref this._isReplayHelperEnabled, value); }
    }

    private int _aggregationUnitSize;

    public int AggregationUnitSize
    {
      get { return this._aggregationUnitSize; }
      private set { this.SetProperty(ref this._aggregationUnitSize, value); }
    }

    private bool _isStartDateChecked;

    public bool IsStartDateChecked
    {
      get { return this._isStartDateChecked; }
      private set { this.SetProperty(ref this._isStartDateChecked, value); }
    }

    private Player _opponentPlayer;

    public Player OpponentPlayer
    {
      get { return this._opponentPlayer; }
      private set { this.SetProperty(ref this._opponentPlayer, value); }
    }

    private Character _opponentCharacter;

    public Character OpponentCharacter
    {
      get { return this._opponentCharacter; }
      private set { this.SetProperty(ref this._opponentCharacter, value); }
    }

    private string _resultMessage;

    public string ResultMessage
    {
      get { return this._resultMessage; }
      private set { this.SetProperty(ref this._resultMessage, value); }
    }

    private Stage _selectedStage;

    public Stage SelectedStage
    {
      get { return this._selectedStage; }
      private set { this.SetProperty(ref this._selectedStage, value); }
    }

    private GameVersion _selectedGameVersion;

    public GameVersion SelectedGameVersion
    {
      get { return this._selectedGameVersion; }
      private set { this.SetProperty(ref this._selectedGameVersion, value); }
    }

    private Result _selectedResult;

    public Result SelectedResult
    {
      get { return this._selectedResult; }
      private set { this.SetProperty(ref this._selectedResult, value); }
    }

    private DateTime _startDate;

    public DateTime StartDate
    {
      get { return this._startDate; }
      private set { this.SetProperty(ref this._startDate, value); }
    }

    private Player _targetPlayer;

    public Player TargetPlayer
    {
      get { return this._targetPlayer; }
      private set { this.SetProperty(ref this._targetPlayer, value); }
    }

    private Character _targetCharacter;

    public Character TargetCharacter
    {
      get { return this._targetCharacter; }
      private set { this.SetProperty(ref this._targetCharacter, value); }
    }

    private ValstatRepository _model;

    public ValstatRepository Repository
    {
      get { return this._model; }
      private set { this.SetProperty(ref this._model, value); }
    }
  }
}
