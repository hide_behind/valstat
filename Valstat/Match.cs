﻿using System;
using System.Linq;
using System.Xml;

namespace HideBehind.ValstatWpfDotNet45Rp
{
  public class Match
  {
    public Match(
      GameVersion gameVersion,
      DateTime date,
      Mode mode,
      string p1Name,
      string p2Name,
      Character p1Character,
      Character p2Character,
      Stage stage,
      string winner,
      string filePath
      )
    {
      _gameVersion = gameVersion;
      _date = date;
      _mode = mode;
      _p1Name = p1Name;
      _p2Name = p2Name;
      _p1Character = p1Character;
      _p2Character = p2Character;
      _stage = stage;
      _winnerName = winner;
      _filePath = filePath;
    }

    private GameVersion _gameVersion;

    public GameVersion GameVersion
    {
      get { return this._gameVersion; }
    }

    private DateTime _date;

    public DateTime Date
    {
      get { return this._date; }
    }

    private Mode _mode;

    public Mode Mode
    {
      get { return _mode; }
    }

    private string _p1Name;

    public string P1Name
    {
      get { return this._p1Name; }
    }

    private Character _p1Character;

    public Character P1Character
    {
      get { return this._p1Character; }
    }

    private string _p2Name;

    public string P2Name
    {
      get { return this._p2Name; }
    }

    private Character _p2Character;

    public Character P2Character
    {
      get { return this._p2Character; }
    }

    private Stage _stage;

    public Stage Stage
    {
      get { return this._stage; }
    }

    private string _winnerName;

    public string WinnerName
    {
      get { return this._winnerName; }
    }

    private string _filePath;

    public string FilePath
    {
      get { return this._filePath; }
      set { this._filePath = value; }
    }

    public static Match ParseNode(XmlNode x, string registeredDirPath)
    {
      GameVersion gameVersion = GameVersion.List.First(y => y.Id.ToString() == x.SelectSingleNode("./@v").Value);
      DateTime dateTime = DateTime.Parse(x.SelectSingleNode("./@d").Value);
      Mode mode = Mode.List.First(y => y.Id.ToString() == x.SelectSingleNode("./@m").Value);
      string p1Name = x.SelectSingleNode("./@n").Value;
      string p2Name = x.SelectSingleNode("./@o").Value;
      var p1Character = Character.List.First(y => y.Id.ToString() == x.SelectSingleNode("./@p").Value);
      var p2Character = Character.List.First(y => y.Id.ToString() == x.SelectSingleNode("./@q").Value);
      Stage stage = Stage.List.First(y => y.Id.ToString() == x.SelectSingleNode("./@s").Value);
      string file = registeredDirPath + "\\V" + gameVersion.Shorthand + x.SelectSingleNode("./@f").Value;
      string winnerNameXPath = "./@w";
      string winnerName = x.SelectSingleNode(winnerNameXPath) == null ?
        String.Empty : x.SelectSingleNode(winnerNameXPath).Value;
      return new Match(gameVersion, dateTime, mode, p1Name, p2Name, p1Character, p2Character, stage, winnerName, file);
    }
  }
}
