﻿【ソフト名】Valstat
【著作権者】hide_behind
【配布元】  電装天使ヴァルフォース Wiki (http://wikiwiki.jp/valforce/) の観戦・リプレイのページ
【動作環境】.NET Framework 4.5 以降がインストール済みの Windows
―――――――――――――――――――――――――――――――――――――
≪著作権および免責事項≫

　本ソフトウェアはフリーソフトウェアです。自由にご使用ください。
なお，著作権は作者である hide_behind が保有しています。

　このソフトウェアを使用したことによって生じたすべての障害・損害・不具合等に関しては、
作者と作者の関係者および作者の所属するいかなる団体・組織とも、一切の責任を負いません。
各自の責任においてご使用ください。

　また、作者は『電装天使ヴァルフォース』の開発元たる夢ソフトと一切の関係はありません。
本ソフトウェアに関する問い合わせを夢ソフトに対し行わないで下さい。


・概要
Valstat (以下、本ソフトウェア) は『電装天使ヴァルフォース』のリプレイ (REP ファイル) の情報をもとに
試合数、勝ち数、負け数、引き分け数を数え上げるプログラムです。
プレイヤー、キャラクター、日時、ステージ、バージョンでの絞り込みができます。
バージョン 2.03 以降のリプレイに対応しています。

・ファイル構成
Zip アーカイブを展開すると以下のファイルが得られます。

Microsoft.WindowsAPICodePack.dll
Microsoft.WindowsAPICodePack.Shell.dll
Microsoft.WindowsAPICodePack.ShellExtensions.dll
ReactiveProperty.DataAnnotations.dll
ReactiveProperty.dll
ReactiveProperty.NET45.dll
ReadMe.txt
System.Reactive.Core.dll
System.Reactive.Interfaces.dll
System.Reactive.Linq.dll
System.Reactive.PlatformServices.dll
System.Windows.Interactivity.dll
Valstat.application
Valstat.exe
Valstat.exe.config
Valstat.exe.manifest

・インストール方法
Zip アーカイブを展開して、得られたファイルを任意の場所に配置します。

・バージョンアップ方法
Zip アーカイブを展開します。旧バージョンのデータベースを引き継ぎたい場合、
旧バージョンのValstatDatabase.xmlをValstat.exe を含むフォルダーにコピーするか、
得られたファイルで旧バージョンを上書きします。
ValstatDatabase.xml の新旧バージョン間の互換性がない場合、自動的にデータベースの再構築を実行します。
状況により非常に重くなります。

・アンインストール方法
1) 本ソフトウェアが起動している場合は終了します。

2) Zip アーカイブを展開して得たファイルを削除します。

3) 設定ファイルを削除します。
   設定ファイルは本ソフトウェアを起動したユーザーのホームディレクトリ内の隠しフォルダ
   AppData\Local\HideBehind 以下に保存されます。

・使い方
1)  Valstat.exe を実行するとウィンドウが開きます。起動しないときは設定ファイルを削除してリトライして下さい。
    設定ファイルは本ソフトウェアを起動したユーザーのホームディレクトリ内の隠しフォルダ
    AppData\Local\HideBehind 以下に保存されます。

2) 「設定」タブを選択します。

3) 「リプレイフォルダー」の入力欄に絶対パスを入力し、「設定の保存」ボタンをクリックします。

4) 「集計」タブを選択し、「データベースの更新」ボタンをクリックするとデータベースの更新が始まります。
    この処理は「検索対象フォルダー」からの再帰的ファイルの取得と各ファイルの読み込み、
    および「登録済みフォルダー」へのファイル移動の処理を含むため、状況によっては非常に重くなります。
    特に初回の更新は未登録リプレイが多いので重くなりがちです。
    初期状態では「設定タブ」で「データベース更新時間 (秒数) の上限を設定する」をチェックされており、
    制限時間 (秒数) が時間切れになると更新を中断するようになっています。
    場合によっては、リプレイの数を一時的に減らしたほうがいいかもしれません。

5)  更新を何度か行ってすべてのリプレイが登録されると「更新の必要はありません」と表示されます。

6)  集計条件を入力します。「集計条件を保存」するをクリックすると次回起動時に自動的に読み込まれます。

7)  集計ボタンを押すと入力内容に応じた結果が表示されます。
    集計ボタンは集計対象のプレイヤー名がセットされているときにのみ表示されます。
    集計の際にあらかじめ設定タブでリプレイ再生支援機能を有効にするをチェックしておくと、
    条件に該当するリプレイの一覧表が表示され再生ボタンで再生できます。
    ただし、REP ファイルが valforce.exe に関連付けされている必要があります。
    バージョン 2.03 の REP ファイルを再生するには
    http://yumesoft.net/valforce/val_dl.html  から、製品版2.03.01パッチをダウンロードし、
    アーカイブを展開して、中の valforce.exe を valforce203.exe として
    ゲーム本体の valforce.exe が含まれているフォルダー (replay フォルダーの上位フォルダー) に配置します。

・履歴
2015-05-15, 1.1.3.17, データグリッドに行番号を追加。データが新着順に表示されないバグの修正
2015-05-13, 1.1.2.16, データベースの軽量化。
2015-04-20, 1.1.1.15, 結果 (詳細) の日時の割り当てミスの修正、リプレイ一覧のキャラクター名表示を変更。
2015-04-15, 1.1.0.14, .NET Framework 4.5 に変更、ユーザー指定の一定試合数ごとに勝率を計算する機能の追加、
                      機能していないオプションの削除
2015-03-29, 1.0.5.13, 引き分けの集計に関するバグの修正、データベースの互換性問題の解決
2015-03-28, 1.0.4.12, 試合結果によるフィルタリング、集計条件、結果をコピー可能に
2015-02-28, 1.0.3.11, リプレイ一覧表に勝者の列を追加 (thanks to zerorain)
2015-02-28, 1.0.2.10, リプレイ再生支援機能の実装、タイムアウト時のメッセージ追加
2015-02-20, 1.0.1.9,  日付指定、アプリケーション起動時におけるプレイヤーリストの更新に関するバグ修正
2015-02-16, 1.0.0.8,  設定 GUI の実装、自動待避機能の実装、引き分けカウントの実装、勝率表示の実装
2015-01-21, 0.0.0.7,  プログレス・バーの実装
2015-01-21, 0.0.0.6,  データベースの最終更新日時が反映されないバグの修正
2015-01-21, 0.0.0.5,  データベースのwinnerCharacter, loserCharacter 属性値に不適切な値をセットするバグ
                       (0.0.0.2 によるエンバグ) を修正。
2015-01-19, 0.0.0.4,  更新時の処理の速度改善他
2015-01-18, 0.0.0.3,  データベース更新の際に新しいファイルのみ読み込むように修正
2015-01-18, 0.0.0.2,  内部処理の変更
2015-01-17, 0.0.0.1,  集計期間の指定無効バグ修正 (thanks to NN_)
2015-01-14, 0.0.0.0,  新規作成 by hide_behind
