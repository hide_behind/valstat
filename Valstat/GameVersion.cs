﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace HideBehind.ValstatWpfDotNet45Rp
{
  public class GameVersion
  {
    public static readonly GameVersion Any
      = new GameVersion { _id = 0, _longhand = "指定しない", _shorthand = "ANY" };

    public static readonly GameVersion V2v03
      = new GameVersion { _id = 1, _longhand = "2.03", _shorthand = "2.03" };

    public static readonly GameVersion V2v04
      = new GameVersion { _id = 2, _longhand = "2.04", _shorthand = "2.04" };

    private int _id;

    public int Id
    {
      get { return _id; }
    }

    private string _longhand;

    public string Longhand
    {
      get { return _longhand; }
    }

    private string _shorthand;

    public string Shorthand
    {
      get { return _shorthand; }
    }

    private static readonly ReadOnlyCollection<GameVersion> _list
      = new ReadOnlyCollection<GameVersion>(new[] { Any, V2v03, V2v04, });

    public static ReadOnlyCollection<GameVersion> List
    {
      get { return _list; }
    }
  }
}
