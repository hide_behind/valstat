﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace HideBehind.ValstatWpfDotNet45Rp
{
  public class Result
  {
    public static readonly Result Any = new Result { _id = 0, _shorthand = "ANY", _longhand = "指定しない", };
    public static readonly Result Win = new Result { _id = 1, _shorthand = "WIN", _longhand = "勝ち", };
    public static readonly Result Loss = new Result { _id = 2, _shorthand = "LOSS", _longhand = "負け", };
    public static readonly Result Draw = new Result { _id = 3, _shorthand = "DRQW", _longhand = "引き分け", };

    private int _id;

    public int Id
    {
      get { return _id; }
    }

    private string _shorthand;

    public string Shorthand
    {
      get { return _shorthand; }
    }

    private string _longhand;

    public string Longhand
    {
      get { return _longhand; }
    }

    private static readonly ReadOnlyCollection<Result> _list
      = new ReadOnlyCollection<Result>(new[] { Any, Win, Loss, Draw, });

    public static ReadOnlyCollection<Result> List
    {
      get { return _list; }
    }
  }
}
