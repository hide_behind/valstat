﻿using HideBehind.ValstatWpfDotNet45Rp.Common;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace HideBehind.ValstatWpfDotNet45Rp
{
  public class Player : BindableBase
  {
    private string _name;

    public string Name
    {
      get { return this._name; }
      set
      {
        this.SetProperty(ref this._name, value);
      }
    }

    private int _frequency;

    public int Frequency
    {
      get { return this._frequency; }
      set
      {
        this.SetProperty(ref this._frequency, value);
      }
    }
  }
}
