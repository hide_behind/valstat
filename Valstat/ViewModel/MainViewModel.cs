﻿using Microsoft.WindowsAPICodePack.Dialogs;
using Reactive.Bindings;
using Reactive.Bindings.Extensions;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;

namespace HideBehind.ValstatWpfDotNet45Rp.ViewModel
{
  public class MainViewModel
  {
    public MainViewModel(Search search)
    {
      this.RefreshDatabase = new ReactiveCommand();
      this.RefreshDatabase.Subscribe(async _ =>
      {
        this.IsStandby.Value = false;
        await Task.Run(() =>
        {
          search.Repository.RefreshDatabase((string)_);
        });
        this.IsStandby.Value = true;
        App.Current.Shutdown();
      });

      this.OpenReplay = new ReactiveCommand();
      this.OpenReplay.Subscribe(async _ =>
      {
        this.IsStandby.Value = false;
        await Task.Run(() =>
        {
          search.Repository.OpenReplay((Match)_);
        });
        this.IsStandby.Value = true;
      });

      this.Aggregate = new ReactiveCommand();
      this.Aggregate.Subscribe(async _ =>
      {
        this.IsStandby.Value = false;
        await Task.Run(() =>
        {
          search.Aggregate();
        });
        this.IsStandby.Value = true;
      });

      this.SaveSetting = new ReactiveCommand();
      this.SaveSetting.Subscribe(async _ =>
      {
        this.IsStandby.Value = false;
        await Task.Run(() =>
        {
          search.Repository.SaveSetting();
        });
        this.IsStandby.Value = true;
      });

      this.SaveSearch = new ReactiveCommand();
      this.SaveSearch.Subscribe(async _ =>
      {
        this.IsStandby.Value = false;
        await Task.Run(() =>
        {
          search.SaveSearch();
        });
        this.IsStandby.Value = true;
      });

      this.ShowFolderPicker = new ReactiveCommand();
      this.ShowFolderPicker.Subscribe(_ =>
      {
        var dialog = new CommonOpenFileDialog();
        dialog.DefaultDirectory = search.Repository.ReplayDirPath;
        dialog.IsFolderPicker = true;
        dialog.EnsureFileExists = true;
        dialog.EnsurePathExists = true;
        dialog.EnsureReadOnly = false;
        dialog.EnsureValidNames = true;
        var result = dialog.ShowDialog();
        if (result == CommonFileDialogResult.Ok)
        {
          this.ReplayDirPath.Value = dialog.FileName;
        }
      });

      this.UpdateDatabase = new ReactiveCommand();
      this.UpdateDatabase.Subscribe(async _ =>
      {
        this.IsStandby.Value = false;
        await Task.Run(() =>
        {
          search.Repository.RefreshDatabase((string)_);
        });
        if (this.TargetPlayer.Value != null)
        {
          search.Aggregate();
        }
        this.IsStandby.Value = true;
      });

      this.AppNameAndVersion = search.Repository.ObserveProperty(x => x.AppNameAndVersion).ToReactiveProperty();
      this.DatabaseStatusMessage = search.Repository.ObserveProperty(x => x.DatabaseStatusMessage).ToReactiveProperty();
      this.Progress = search.Repository.ObserveProperty(x => x.Progress).ToReactiveProperty();
      this.ResultMessage = search.ObserveProperty(x => x.ResultMessage).ToReactiveProperty();

      this.AggregationUnitSize = search.ToReactivePropertyAsSynchronized(x => x.AggregationUnitSize);
      this.EndDate = search.ToReactivePropertyAsSynchronized(x => x.EndDate);
      this.IsAnalysisEnabled = search.ToReactivePropertyAsSynchronized(x => x.IsAnalysisEnabled);
      this.IsEndDateChecked = search.ToReactivePropertyAsSynchronized(x => x.IsEndDateChecked);
      this.IsOpponentNameChecked = search.ToReactivePropertyAsSynchronized(x => x.IsOpponentNameChecked);
      this.IsReplayHelperEnabled = search.ToReactivePropertyAsSynchronized(x => x.IsReplayHelperEnabled);
      this.IsStandby = search.Repository.ToReactivePropertyAsSynchronized(x => x.IsStandby);
      this.IsStartDateChecked = search.ToReactivePropertyAsSynchronized(x => x.IsStartDateChecked);
      this.IsTimeLimitEnabled = search.Repository.ToReactivePropertyAsSynchronized(x => x.IsTimeLimitEnabled);
      this.OpponentCharacter = search.ToReactivePropertyAsSynchronized(x => x.OpponentCharacter);
      this.OpponentPlayer = search.ToReactivePropertyAsSynchronized(x => x.OpponentPlayer);
      this.ReplayDirPath = search.Repository.ToReactivePropertyAsSynchronized(x => x.ReplayDirPath);
      this.SelectedGameVersion = search.ToReactivePropertyAsSynchronized(x => x.SelectedGameVersion);
      this.SelectedResult = search.ToReactivePropertyAsSynchronized(x => x.SelectedResult);
      this.SelectedStage = search.ToReactivePropertyAsSynchronized(x => x.SelectedStage);
      this.StartDate = search.ToReactivePropertyAsSynchronized(x => x.StartDate);
      this.TargetCharacter = search.ToReactivePropertyAsSynchronized(x => x.TargetCharacter);
      this.TargetPlayer = search.ToReactivePropertyAsSynchronized(x => x.TargetPlayer);
      this.TimeLimit = search.Repository.ToReactivePropertyAsSynchronized(x => x.TimeLimit);

      this.MatchList = search.MatchList.ToReadOnlyReactiveCollection();
      this.PlayerList = search.Repository.PlayerList.ToReadOnlyReactiveCollection();
      this.StatisticsList = search.StatisticsList.ToReadOnlyReactiveCollection();

      this.Characters = new ReadOnlyCollection<Character>(Character.List);
      this.GameVersions = new ReadOnlyCollection<GameVersion>(GameVersion.List);
      this.Results = new ReadOnlyCollection<Result>(Result.List);
      this.Stages = new ReadOnlyCollection<Stage>(Stage.List);
    }

    public ReactiveCommand SaveSearch { get; private set; }

    public ReactiveCommand SaveSetting { get; private set; }

    public ReactiveCommand RefreshDatabase { get; private set; }

    public ReactiveCommand Aggregate { get; private set; }

    public ReactiveCommand OpenReplay { get; private set; }

    public ReactiveCommand ShowFolderPicker { get; private set; }

    public ReactiveCommand UpdateDatabase { get; private set; }

    public ReactiveProperty<string> AppNameAndVersion { get; private set; }

    public ReactiveProperty<string> DatabaseStatusMessage { get; private set; }

    public ReactiveProperty<double> Progress { get; private set; }

    public ReactiveProperty<string> ResultMessage { get; private set; }

    public ReactiveProperty<int> AggregationUnitSize { get; private set; }

    public ReactiveProperty<DateTime> EndDate { get; private set; }

    public ReactiveProperty<bool> IsAnalysisEnabled { get; private set; }

    public ReactiveProperty<bool> IsEndDateChecked { get; private set; }

    public ReactiveProperty<bool> IsOpponentNameChecked { get; private set; }

    public ReactiveProperty<bool> IsReplayHelperEnabled { get; private set; }

    public ReactiveProperty<bool> IsStartDateChecked { get; private set; }

    public ReactiveProperty<bool> IsStandby { get; private set; }

    public ReactiveProperty<bool> IsTimeLimitEnabled { get; private set; }

    public ReactiveProperty<Player> OpponentPlayer { get; private set; }

    public ReactiveProperty<Character> OpponentCharacter { get; private set; }

    public ReactiveProperty<string> ReplayDirPath { get; private set; }

    public ReactiveProperty<GameVersion> SelectedGameVersion { get; private set; }

    public ReactiveProperty<Result> SelectedResult { get; private set; }

    public ReactiveProperty<Stage> SelectedStage { get; private set; }

    public ReactiveProperty<DateTime> StartDate { get; private set; }

    public ReactiveProperty<Player> TargetPlayer { get; private set; }

    public ReactiveProperty<Character> TargetCharacter { get; private set; }

    public ReactiveProperty<double> TimeLimit { get; private set; }

    public ReadOnlyCollection<Character> Characters { get; private set; }

    public ReadOnlyCollection<GameVersion> GameVersions { get; private set; }

    public ReadOnlyCollection<Result> Results { get; private set; }

    public ReadOnlyCollection<Stage> Stages { get; private set; }

    public ReadOnlyReactiveCollection<Match> MatchList { get; private set; }

    public ReadOnlyReactiveCollection<Player> PlayerList { get; private set; }

    public ReadOnlyReactiveCollection<Statistics> StatisticsList { get; private set; }

    public Search searchModel { get; private set; }
  }
}
