﻿using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace HideBehind.ValstatWpfDotNet45Rp.Common
{
  public class BindableBase : INotifyPropertyChanged
  {
    public event PropertyChangedEventHandler PropertyChanged;
    protected void SetProperty<T>(ref T field, T value, [CallerMemberName]string propertyName = null)
    {
      if (object.Equals(field, value)) { return; }
      field = value;
      var h = this.PropertyChanged;
      if (h != null) { h(this, new PropertyChangedEventArgs(propertyName)); }
    }
  }
}
