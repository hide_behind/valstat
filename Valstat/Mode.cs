﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace HideBehind.ValstatWpfDotNet45Rp
{
  public class Mode
  {
    public static readonly Mode Arcade
      = new Mode { _id = 0, _longhand = "ARCADE MODE", _shorthand = "ARCADE" };

    public static readonly Mode Practice
      = new Mode { _id = 1, _longhand = "PRACTICE MODE", _shorthand = "PRACTICE" };

    public static readonly Mode Network
      = new Mode { _id = 2, _longhand = "NETWORK MODE", _shorthand = "NETWORK" };

    private int _id;

    public int Id
    {
      get { return _id; }
    }

    private string _longhand;

    public string Longhand
    {
      get { return _longhand; }
    }

    private string _shorthand;

    public string Shorthand
    {
      get { return _shorthand; }
    }

    private static readonly ReadOnlyCollection<Mode> _list
      = new ReadOnlyCollection<Mode>(new[] { Arcade, Practice, Network, });

    public static ReadOnlyCollection<Mode> List
    {
      get { return _list; }
    }
  }
}
